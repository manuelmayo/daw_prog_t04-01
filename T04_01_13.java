//T04_01_13
public class T04_01_13 {
	
	public static void main(String[] args) {
		
		if (args.length==2) {
			
			try {
				double num1 = Double.parseDouble(args[0]);
				double num2 = Double.parseDouble(args[1]);
				
				System.out.printf("> %.2f\n",num1+num2);
				
			} catch(NumberFormatException e) {
				System.out.println(e);
			}
			
		} else {	
			System.out.println("Debes pasar 2 argumentos numéricos");
		}
		
	}
	
}
