//T04_01_10
public class T04_01_10 {
	
	public static void main(String[] args) {
		
		System.out.println("Conversor de PULGADAS a METROS [bucle]:\n");
		
		System.out.println("PULGADAS | METROS\n");
		
		for (int i=1;i<=100;i++) {
			
			String string_pm = String.format("%03d %6s %.2f", 
						i, "|", i/39.38);

			System.out.println(string_pm);
			
			if (i%12 == 0) {
				System.out.println("");
			}
			
		}
		
	}

}
