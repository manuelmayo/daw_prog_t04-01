//T04_01_12
public class T04_01_12 {

	public static void main(String[] args) {
		
		float peso;
		
		if (args.length > 0) {
			
			try {
				peso = Float.parseFloat(args[0]);
				
				float peso_luna = (float) (peso*0.17);
				
				System.out.println("Peso en la tierra: "+peso);
				System.out.println("Peso en la luna: "+ peso_luna);
				
			} catch(NumberFormatException e) {
				System.out.printf("El valor '%s' no es un peso v�lido\n",args[0]);
			}
			
		} else {	
			System.out.println("Debes pasar un peso como argumento");
		}
		
	}
	
}
